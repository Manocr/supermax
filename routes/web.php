<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/admin/login', 'Admin\HomeController@login');

Route::post('/admin_login', 'Admin\HomeController@admin_login');

Route::post('/forgot_mail', 'Admin\HomeController@forgot_mail');

Route::get('/admin/offers_list', 'Admin\HomeController@offers_list');

Route::post('/update_offers', 'Admin\HomeController@update_offers');

Route::post('/ChangeStatus', 'Admin\HomeController@ChangeStatus');

Route::get('/admin_logout', 'Admin\HomeController@admin_logout');

Route::get('/admin/dashboard', 'Admin\HomeController@dashboard');

Route::get('/admin/contact_list', 'Admin\HomeController@contact_list');

Route::group(['prefix' => '/courier'], function () {

    Route::get('/list', 'Admin\HomeController@courier_list');

    Route::get('/add_courier', 'Admin\HomeController@add_courier');

    Route::get('/edit_courier/{id}', 'Admin\HomeController@edit_courier');

    Route::get('/delete_courier/{id}', 'Admin\HomeController@delete_courier');

    Route::post('/store_courier', 'Admin\HomeController@store_courier');

    Route::post('/update_courier', 'Admin\HomeController@update_courier');

    Route::get('/delete_contact/{id}', 'Admin\HomeController@delete_contact');

});

Route::group(['prefix' => '/gallery'], function () {

    Route::get('/list', 'Admin\HomeController@gallery_list');

    Route::post('/store_gallery', 'Admin\HomeController@store_gallery');

    Route::get('/delete_gallery/{id}', 'Admin\HomeController@delete_gallery');

});




// UI
Route::get('/', 'UI\HomeController@home');

Route::get('/track_courier', 'UI\HomeController@track_courier');

Route::post('/search_courier', 'UI\HomeController@search_courier');

Route::get('/services', 'UI\HomeController@services');

Route::get('/contact', 'UI\HomeController@contact');

Route::get('/gallery_list', 'UI\HomeController@gallery_list');

Route::post('/store_contact', 'UI\HomeController@store_contact');
