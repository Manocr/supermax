{{-- <a href="https://wa.me/+96555407640" target="_blank" class="float">
    <i class="fa fa-whatsapp fa-3x my-float"></i>
    </a> --}}

<footer>
    <div class="big_footer_wrapper section_padding">
        <div class="container">
            <div class="row">

                <div class="col-md-3 xxs_fullwidth col-xs-6">
                    <div class="footer_about_wrapper reveal animated" data-anim="fadeInLeftShort" >
                        <div class="footer_social">
                            <h4>get connected</h4>
                            <ul>
                                <li><a href="https://www.facebook.com/profile.php?id=100005727916426" target="_blank"><span class="fa fa-facebook"></span></a></li>
                                 <li><a href="https://www.instagram.com/supermaxcargo/" target="_blank"><span class="fa fa-instagram"></span></a></li>
                                <li><a href="https://twitter.com/CargoSupermax?s=08"><span class="fa fa-twitter"></span></a></li>
                               {{-- <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                <li><a href="#"><span class="fa fa-skype"></span></a></li> --}}
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 xxs_fullwidth col-xs-6">
                    <div class="footer_widgets contact reveal animated" data-anim="fadeInRightShort" data-delay="0.4s">
                        <div class="widget_title">
                            <h4>contact us</h4>
                        </div>
                        <div class="footer_address">
                            <ul>
                                <li><span class="fa fa-paper-plane-o"></span> <div class="address_right">Faisal Complex, Abraq Kaithan - Kuwait.</div></li>
                                <li>
                                    <span class="fa fa-clock-o"></span>
                                    <div class="address_right">
                                        <p>Sunday To Saturday</p>
                                        <p>9Am To 8pm</p>
                                    </div>
                                </li>
                                <li>
                                    <span class="fa fa-phone"></span>
                                    <div class="number address_right">
                                        <a href="tel:+965 554 07640">+965 5540 7640</a>
                                        <a href="tel:+965 667 13566">+965 6671 3566</a>
                                        <a href="tel:+965 977 82015">+965 9778 2015</a>
                                        {{-- <a href="tel:01212347794">+012 (2345) 9874</a> --}}
                                    </div>
                                </li>
                                <li>
                                    <span class="fa fa-envelope-o"></span>
                                    <div class="address_right">
                                        <a href="mailto:mdgani@supermaxcargo.com">mdgani@supermaxcargo.com</a>
                                    </div>
                                </li>
                                <li>
                                    <span class="fa fa-globe"></span>
                                    <div class="address_right">
                                        <a href="http://supermaxcargo.com/">www.supermaxcargo.com</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 xxs_fullwidth col-xs-6">
                    <div class="footer_about_wrapper reveal animated" data-anim="fadeInLeftShort" >
                        <a href="/UI/Super_max_Cargo.apk" download><img class="width100" src="{{ URL::asset('UI/images/android_app_btn.png') }}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tiny_footer">
        <div class="container">
            <div class="col-md-6 xs_fullwidth col-xs-6">
                <div class="footer_text_wrapper">
                    <p class="footer_text">Copyright © 2020 Supermax Cargo. All Rights Reserved by <a href="/">Supermax Cargo</a></p>
                </div>
            </div>
            {{-- <div class="col-md-6 xs_fullwidth col-xs-6">
                <div class="footer_menu clearfix">
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="about_us.html">About Us</a></li>
                        <li><a href="track_trace.html">Track & Trace</a></li>
                        <li><a href="blog.html">News</a></li>
                        <li><a href="contcat.html">Contact Us</a></li>
                    </ul>
                </div>
            </div> --}}
        </div>
    </div>
</footer>