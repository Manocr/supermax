<div class="menu_area">

    <!-- container starts -->
    <div class="container">
        <!-- row start -->
        <div class="row">
            <div class="col-md-12">
                <div class="mainmenu nav_shadow">
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="nav navbar-nav magic_menu">
                                        <li class="has_dropdown active">
                                            <a href="/">home</a>
                                        </li>
                                        <li class="has_dropdown ">
                                            <a href="/services">Service</a>
                                        </li>
                                        <li class="has_dropdown ">
                                            <a href="/track_courier">Track & Trace</a>
                                        </li>
                                        <li class="has_dropdown ">
                                            <a href="/gallery_list">Gallery</a>
                                        </li>
                                        <li><a href="/contact">contact</a></li>
                                        <li style="background: rgb(152, 17, 19);"><a href="https://wa.me/+96555407640" style="
                                            color: #fff!important;
                                        ">Cargo Booking</a></li>
                                    </ul>
                                </div>
                            
                                {{-- <div class="col-md-5">
                                    <div class="tc_form hide_mobile_quote">
                                        <form action="/search_courier" method="post">
                                            @csrf
                                            <div class="tc_input_wrapper">
                                                <input name="bill_no" type="text" placeholder="Track your bill no">
                                                <span class="fa fa-truck"></span>
                                                <button class="tc_btn" type="submit">Search</button>
                                            </div>
                                        </form>
                                    </div>
                                </div> --}}
                            </div>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div><!-- main menu ends -->
            </div>
        </div><!-- /.row end -->

    </div><!-- /.container ends -->
</div>