<header>
    <!-- container start -->
    <div class="container">
        <!-- .row start -->
        {{-- <div class="row">
            <div class="tiny_header clearfix">
                <div class="col-md-12">
                    <div class="tiny_header_wrapper">
                        <div class="header_info">
                            <ul>
                                <li><a href="/track_courier">Track Shipment</a></li>
                            </ul>
                        </div>

                        <div class="times">
                            <p><i class="fa fa-clock-o"></i> <span class="day">Sat - Thus</span>9 am - 6 pm</p>
                        </div>

                        <div class="social_links">
                            <ul>
                                <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- /.row end -->

        <!-- row start -->
        <div class="row">
            <div class="header_middle_wrapper clearfix">
                <div class="col-md-2 col-lg-3 xs_fullwidth col-xs-3">
                    <div class="logo_container">
                        <a href="/"><img class="logo" src="{{ URL::asset('UI/images/logo.jpg') }}" alt="logo Here"></a>
                    </div>
                </div>

                <div class="col-lg-4 xs_fullwidth col-xs-6 col-md-6 col-md-offset-0">
                    <div class="contact_info">
                        {{-- <div class="single_info_section">
                            <span class="fa fa-headphones v_middle"></span>
                            <div class="contact_numbers v_middle right_info">
                                <p><a href="#"> +965 5540 7640, 6671 3566, 9778 2015</a></p>
                            </div>
                        </div> --}}
                        <div class="single_info_section">
                            {{-- <span class="fa fa-envelope v_middle"></span> --}}
                            <div class="contact_numbers right_info v_middle">
                                <p><a href="#"> <i class="fa fa-phone fa-2x"></i> +965 5540 7640 / 6671 3566 / 9778 2015</a></p>
                                <br>
                                <p><a href="mailto:Supermaxcargokwi@gmail.com"> <i class="fa fa-envelope fa-2x"></i> Supermaxcargokwi@gmail.com</a></p>
                                <br>
                                <p><a href="https://wa.me/+96555407640"> <i class="fa fa-whatsapp fa-2x" style="
                                    color: #5bf877;
                                "></i> Get Enquiry</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-5 xs_fullwidth col-xs-3 col-lg-5">
                    <div class="tc_form">
                        <form action="/search_courier" method="post">
                            @csrf
                            <div class="tc_input_wrapper">
                                <input name="bill_no" type="text" placeholder="Track your bill no" required>
                                {{-- <span class="fa fa-plane"></span> --}}
                                <button class="tc_btn" type="submit">Search</button>
                            </div>
                        </form>
                    </div>

                    {{-- <a href="/contact" class="trust_btn quote_btn hide_mobile_quote">get a free quote</a> --}}
                </div>
            </div>
        </div><!-- /.row end -->


    </div><!-- /.container end -->
</header>
