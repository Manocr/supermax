@extends('UI.base')

@section('CssScript')
@if($Offers)
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
@endif
@endsection

@section('Content')
  <!--================================
        START SLIDER AREA
    =================================-->
    <section class="slider_area">

      <!-- slider starts  -->
      <div class="sliders">

          <!-- hero slides -->
          <div class="hero_sliders">

              <!-- single hero_slide -->
              <div class="hero_slide" data-src="{{ URL::asset('UI/images/banner1.jpg') }}">
                  <div class="captions_wrapper right">
                      <div class="container">
                          <div class="row">
                              <div class="col-md-8 col-md-offset-4">
                                  <div class="single_slider_wrapper">
                                      <span class="small_title fadeInRightShort animated">Freight and Forwarding</span><br>
                                      <h1 class="big_title fadeInRightShort animated">Anywhere in this world</h1>
                                      <div class="hero_btn">
                                          <a href="/services" class="trust_btn">see more</a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div><!-- single hero_slide -->

              <!-- single hero_slide -->
              <div class="hero_slide" data-src="{{ URL::asset('UI/images/banner2.jpg') }}">
                  <div class="captions_wrapper left">
                      <div class="container">
                          <div class="row">
                              <div class="col-md-8">
                                  <div class="single_slider_wrapper">
                                      <span class="small_title fadeInLeftShort animated">Every villages and towns  </span><br>
                                      <h1 class="big_title fadeInLeftShort animated">Door To Door All over india Air & Sea cargo</h1>
                                      <div class="hero_btn">
                                          <a href="/services" class="trust_btn">see more</a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div><!-- single hero_slide -->

          </div><!-- ./hero slides  -->
      </div><!-- /.sliders ends -->

      <!-- menu starts -->
      @include('UI.common.menu')
      <!-- menu ends -->
  </section>
  <!--================================
      END SLIDER AREA
  =================================-->

  <!--================================
      START SERVICE AREA
  =================================-->
  <section class="service_area section_padding reveal animated" data-anim="fadeInUpShort">
      <!-- container start -->
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <!-- section_title starts -->
                  <div class="section_title">
                      {{-- <div class="sub_title">
                          <p>Service We Provide</p>
                      </div> --}}
                      <div class="title"><h2>Service We Provide</h2></div>
                  </div><!-- section_title starts -->
              </div>
          </div>

          <!-- row start -->
          <div class="row">
              <!-- col start -->
              <div class="col-md-12">

                  <!-- start service wrapper -->
                  <div class="service_wrapper">

                      <!-- service slider start -->
                      <div class="service_slider">

                          <div class="single_service_wrapper">
                              <div class="service_img">
                                  <img src="{{ URL::asset('UI/images/express_courier.jpg') }}" class="services-box" alt="service-img">
                              </div>
                              <div class="service_content">
                                  <div class="service_title">
                                      <a href="/services"><h3>EXPRESS COURIER</h3></a>
                                  </div>
                                  {{-- <div class="service_text">
                                      <p>Claritas est etiam processus dynamicus sequitur the mutationem  lectorum Mirum est..  </p>
                                  </div> --}}
                                  <div class="read_more">
                                      <a href="/services">read more <span class="fa fa-long-arrow-right"></span></a>
                                  </div>
                              </div>
                          </div>

                          <div class="single_service_wrapper">
                              <div class="service_img">
                                  <img src="{{ URL::asset('UI/images/air_cargo.jpg') }}" class="services-box" alt="service-img">
                              </div>
                              <div class="service_content">
                                  <div class="service_title">
                                      <a href="/services"><h3>AIR CARGO</h3></a>
                                  </div>
                                  {{-- <div class="service_text">
                                      <p>Claritas est etiam processus dynamicus sequitur the mutationem  lectorum Mirum est..  </p>
                                  </div> --}}
                                  <div class="read_more">
                                      <a href="/services">read more <span class="fa fa-long-arrow-right"></span></a>
                                  </div>
                              </div>
                          </div>

                          <div class="single_service_wrapper">
                              <div class="service_img">
                                  <img src="{{ URL::asset('UI/images/service4.jpg') }}" class="services-box" alt="service-img">
                              </div>
                              <div class="service_content">
                                  <div class="service_title">
                                      <a href="/services"><h3>SEA CARGO</h3></a>
                                  </div>
                                  {{-- <div class="service_text">
                                      <p>Claritas est etiam processus dynamicus sequitur the mutationem  lectorum Mirum est..  </p>
                                  </div> --}}
                                  <div class="read_more">
                                      <a href="/services">read more <span class="fa fa-long-arrow-right"></span></a>
                                  </div>
                              </div>
                          </div>
                      </div><!-- service slider start -->

                      <!-- slider control start -->
                      <div class="slider_controller">
                          <div class="prev"><span class="fa fa-angle-left"></span></div>
                          <div class="next"><span class="fa fa-angle-right"></span></div>
                      </div><!-- slider control end -->

                  </div><!-- start service wrapper -->
              </div><!-- /.col ends -->
          </div><!-- /.row end -->
      </div><!-- /.container start -->
  </section>
  <!--================================
      END SERVICE AREA
  =================================-->

  <!--================================
      START WHY CHOOSE US AREA
  =================================-->
  <section class="feature_area dark_bg reveal animated" data-delay="0.2s" data-anim="fadeInUpShort">

      <!-- container -->
      <div class="container">
          <div class="col-md-6 xs_fullwidth col-xs-6 features_bg">
              <div class="feature_wrapper section_padding">
                  <!-- section_title starts -->
                  <div class="section_title">
                      <div class="sub_title">
                          <p>Our Best Features</p>
                      </div>
                      <div class="title"><h2>why choose us</h2></div>
                  </div><!-- section_title starts -->

                  <div class="about_us_text">
                      <p>Supermax cargo has an efficient team of management professionals who has more than 10 years of experience in the cargo and courier industry; Our executive team has the expertise to deliver for you. Mohamed Gani Abdhul Rahman is the CEO of the company and has more than 10+ years experience in the courier and cargo industry. We bring with us a core group of talented managers and delivery professionals that are second to none in the business community.</p>
                  </div>

                  <div class="section_title">
                    <div class="title"><h2>Vision</h2></div>
                </div><!-- section_title starts -->

                <div class="about_us_text">
                    <p>Our vision is to establish long-lasting excellence in delivery capabilities focused on the individual customer and to create an organization with opportunities to the deserving in owning their own business at their local area, thereby ensuring a Customer Courier bonding with the best of services and make it the goal of our organization.</p>
                </div>

                  <div class="single_feature">
                      <div class="feature_icon">
                          <span class="fa fa-support"></span>
                      </div>
                      <div class="feature_content">
                          <div class="feature_title">
                              <h4>100% Safe Delivery</h4>
                          </div>
                          {{-- <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p> --}}
                          <p></p>
                      </div>
                  </div>

                  <div class="single_feature">
                      <div class="feature_icon">
                          <span class="fa fa-clock-o"></span>
                      </div>
                      <div class="feature_content">
                          <div class="feature_title">
                            <h4>Fast & On Time Delivery</h4>

                          </div>
                          {{-- <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p> --}}
                          <p></p>
                      </div>
                  </div>

                  <div class="single_feature">
                      <div class="feature_icon">
                          <span class="fa fa-plane"></span>
                      </div>
                      <div class="feature_content">
                          <div class="feature_title">
                            <h4>world wide delivery</h4>
                          </div>
                          {{-- <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p> --}}
                          <p></p>
                      </div>
                  </div>
              </div>
          </div>
      </div><!-- /.container ends -->

      <!-- feature starts -->
      <div class="feature_img"></div><!-- feature ends -->

  </section>
  <!--================================
      END WHY CHOOSE US AREA
  =================================-->



  <!--================================
      START CALL TO ACTION
  =================================-->
  <section class="call_to_action reveal animated" data-delay="0.2s" data-anim="fadeInUpShort">
      <div class="container">
          <div class="col-md-7 xs_fullwidth col-xs-8 v_middle">
              <div class="call_to_action_title">
                  <h3>Our special service 24/7</h3>
              </div>
              {{-- <p>Placerat facer possim assum Typi non habent s legentis in iis qui facit eorum</p> --}}
          </div>
          <div class="col-md-5 xs_fullwidth col-xs-3 v_middle">
              <div class="call_to_action_btn">
                  <a class="trust_btn" href="https://wa.me/+96597782015"><i class="fa fa-whatsapp fa-2x" style="
                    color: #5bf877;
                "></i> Whatsapp</a>
              </div>
          </div>
      </div>
  </section>
  <!--================================
      END CALL TO ACTION
  =================================-->
  @if($Offers)
  <div id="popModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img src="/Admin/offers/{{ $Offers->image }}" class="img-responsive">
            </div>
        </div>
      </div>
  </div>
  @endif
@endsection

@section('JSScript')
    <script>
        $(document).ready( function () {
            $("#popModal").modal('show');
} );
    </script>
@endsection
