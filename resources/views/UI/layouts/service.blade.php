@extends('UI.base')
@section('Content')
<!--================================
        START SLIDER AREA
    =================================-->
    <section class="breadcrumb services">

        <div class="breadcrumb_content">
            <!-- container starts -->
            <div class="container">
                <!-- row starts -->
                <div class="row">
                    <!-- col-md-12 starts -->
                    <div class="col-md-12">
                        <div class="breadcrumb_title_wrapper">
                            <div class="page_title">
                                <h1>our services</h1>
                            </div>
                            <ul class="bread_crumb">
                                <li><a href="/">Home</a></li>
                                <li class="bread_active">Our Services</li>
                            </ul>
                        </div>
                    </div><!-- col-md-12 ends -->
                </div>
                <!-- /.row ends -->
            </div><!-- /.container ends -->
        </div>

        <!-- menu starts -->
        @include('UI.common.menu')
        <!-- menu ends -->
    </section>
    <!--================================
        END SLIDER AREA
    =================================-->
<!--================================
        START ABOUT US AREA
    =================================-->
    <section class="services_section section_padding reveal animated" data-delay="0.2s" data-anim="fadeInUpShort">
        <!-- container starts -->
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-6 xxs_fullwidth">
                    <div class="single_service_wrapper">
                        <div class="service_img">
                            <img src="{{ URL::asset('UI/images/express_courier.jpg') }}" class="services-box" alt="service-img">
                        </div>
                        <div class="service_content bottom10">
                            <div class="service_title">
                                <a href="#"><h3>EXPRESS COURIER</h3></a>
                            </div>
                            {{-- <div class="read_more">
                                <a href="#">read more <span class="fa fa-long-arrow-right"></span></a>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-6 xxs_fullwidth">
                    <div class="single_service_wrapper">
                        <div class="service_img">
                            <img src="{{ URL::asset('UI/images/air_cargo.jpg') }}" class="services-box" alt="service-img">
                        </div>
                        <div class="service_content bottom10">
                            <div class="service_title">
                                <a href="#"><h3>AIR CARGO</h3></a>
                            </div>
                            {{-- <div class="read_more">
                                <a href="#">read more <span class="fa fa-long-arrow-right"></span></a>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-6 xxs_fullwidth">
                    <div class="single_service_wrapper">
                        <div class="service_img">
                            <img src="{{ URL::asset('UI/images/service4.jpg') }}" class="services-box" alt="service-img">
                        </div>
                        <div class="service_content bottom10">
                            <div class="service_title">
                                <a href="#"><h3>SEA CARGO</h3></a>
                            </div>
                            {{-- <div class="read_more">
                                <a href="#">read more <span class="fa fa-long-arrow-right"></span></a>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class="col-md-4 col-xs-6 xxs_fullwidth">
                    <div class="single_service_wrapper">
                        <div class="service_img">
                            <img src="{{ URL::asset('UI/images/port_port.png') }}" class="services-box" alt="service-img">
                        </div>
                        <div class="service_content bottom10">
                            <div class="service_title">
                                <a href="#"><h3>PORT TO PORT WORLD WIDE</h3></a>
                            </div>
                            {{-- <div class="read_more">
                                <a href="#">read more <span class="fa fa-long-arrow-right"></span></a>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-6 xxs_fullwidth">
                    <div class="single_service_wrapper">
                        <div class="service_img">
                            <img src="{{ URL::asset('UI/images/fl_booking.jpg') }}" class="services-box" alt="service-img">
                        </div>
                        <div class="service_content bottom10">
                            <div class="service_title">
                                <a href="#"><h3>TICKET BOKING</h3></a>
                            </div>
                            {{-- <div class="read_more">
                                <a href="#">read more <span class="fa fa-long-arrow-right"></span></a>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-6 xxs_fullwidth">
                    <div class="single_service_wrapper">
                        <div class="service_img">
                            <img src="{{ URL::asset('UI/images/mekka.jpg') }}" class="services-box" alt="service-img">
                        </div>
                        <div class="service_content bottom10">
                            <div class="service_title">
                                <a href="#"><h3>PILGRIMS</h3></a>
                            </div>
                            {{-- <div class="read_more">
                                <a href="#">read more <span class="fa fa-long-arrow-right"></span></a>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div><!-- /.row end -->
        </div>
        <!-- /.container starts -->
    </section>
    <!--================================
        END ABOUT US AREA
    =================================-->


    
    @endsection