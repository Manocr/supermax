@extends('UI.base')
@section('Content')
 <!--================================
        START BREADCRUMB AREA
    =================================-->
    <section class="breadcrumb">

      <div class="breadcrumb_content">
          <!-- container starts -->
          <div class="container">
              <!-- row starts -->
              <div class="row">
                  <!-- col-md-12 starts -->
                  <div class="col-md-12">
                      <div class="breadcrumb_title_wrapper">
                          <div class="page_title">
                              <h1>track & trace</h1>
                          </div>
                          <ul class="bread_crumb">
                              <li><a href="/">Home</a></li>
                              <li class="bread_active">Tracking</li>
                          </ul>
                      </div>
                  </div><!-- col-md-12 ends -->
              </div>
              <!-- /.row ends -->
          </div><!-- /.container ends -->
      </div>

      <!-- menu starts -->
      @include('UI.common.menu')
      <!-- menu ends -->
  </section>
  <!--================================
      END BREADCRUMB AREA
  =================================-->

  <!--================================
      START TRACK & TRACE AREA
  =================================-->
  <section class="tc_section section_padding reveal animated" data-delay="0.2s" data-anim="fadeInUpShort">
      <div class="container">
          <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="tc_title"><h4>We are fast & safe delivery your goods everytime on your door steps</h4></div>
                  <div class="tc_form">
                      <form action="/search_courier" method="post">
                        @csrf
                          <div class="tc_input_wrapper">
                              <input name="bill_no" type="text" placeholder="Track your bill no" required>
                              {{-- <span class="fa fa-truck"></span> --}}
                              <button class="tc_btn" type="submit">Search</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
      <br>
      {{-- <div class="table-responsive">
      <table class="table table-bordered table-checkable" id="CourierList">
         <thead>
             <tr>
                 <th>Pickup Date</th>
                 <th>Bill No</th>
                 <th>Consignment No</th>
                 <th>Cargo Mode</th>
                 <th>Origin </th>
                 <th>Destination</th>
                 <th>Sender</th>
                 <th>Receiver Address</th>
                 <th>Total Pcs</th>
                 <th>Pc Weight</th>
                 <th>Total Weight</th>
                 <th>Courier Status</th>
                 <th>Assured Delivery Date</th>
                 <th>Delivered Date</th>
                 <th>Hub Id</th>
                 <th>Doucket No</th>
                 <th>Remarks</th>
             </tr>
         </thead>
         <tbody>

         </tbody>
     </table>
      </div> --}}
  </section>
  <!--================================
      END TRACK & TRACE AREA
  =================================-->


@endsection
