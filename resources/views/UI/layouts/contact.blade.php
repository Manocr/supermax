@extends('UI.base')
@section('Content')
 <!--================================
        START BREADCRUMB AREA
    =================================-->
    <section class="breadcrumb">

        <div class="breadcrumb_content">
            <!-- container starts -->
            <div class="container">
                <!-- row starts -->
                <div class="row">
                    <!-- col-md-12 starts -->
                    <div class="col-md-12">
                        <div class="breadcrumb_title_wrapper">
                            <div class="page_title">
                                <h1>contact us</h1>
                            </div>
                            <ul class="bread_crumb">
                                <li><a href="/">Home</a></li>
                                <li class="bread_active">Contact</li>
                            </ul>
                        </div>
                    </div><!-- col-md-12 ends -->
                </div>
                <!-- /.row ends -->
            </div><!-- /.container ends -->
        </div>

        <!-- menu starts -->
        @include('UI.common.menu')
        <!-- menu ends -->
    </section>
    <!--================================
        END BREADCRUMB AREA
    =================================-->

    <!--================================
        START QUOTE AREA
    =================================-->
    <section class="get_quote section_padding reveal animated" data-delay="0.2s" data-anim="fadeInUpShort">
        <!-- container starts -->
        <div class="container">
            <!-- row starts -->
            <div class="row">

                <!-- col-md-3 starts -->
                <div class="col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                    <div class="quote_requ_wrapper">
                        <div class="section_title title_center">
                            <div class="title"><h2>SEND US A MESSAGE</h2></div>
                        </div>

                        <div class="contact_form">
                            @csrf
                            @if(session('message'))
                                <div class="alert alert-success">
                                    <ul>
                                        <li>{!! session('message') !!}</li>
                                    </ul>
                                </div>
                            @endif
                            <form action="/store_contact" method="post">
                                @csrf
                                <div class="form_half left">
                                    <input type="text" placeholder="Name" name="name">
                                </div>
                                <div class="form_half right">
                                    <input type="text" placeholder="Phone" name="mobile">
                                </div>

                                <input type="email" placeholder="Email" name="email">

                                <textarea name="message" placeholder="Message" cols="30" rows="10"></textarea>

                                <div class="contact_btn_wrapper">
                                    <button class="trust_btn qute_sbmt" type="submit" name="button">send message</button>
                                </div>
                            </form>
                        </div>
                    </div><!--  -->
                </div><!-- col-md-3 ends -->
            </div><!-- /.row ends -->
        </div><!-- container ends -->
    </section>
    <!--================================
        END QUOTE AREA
    =================================-->

    @endsection