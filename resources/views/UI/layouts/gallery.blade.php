@extends('UI.base')
@section('Content')
 <!--================================
        START BREADCRUMB AREA
    =================================-->
    <section class="breadcrumb">

        <div class="breadcrumb_content">
            <!-- container starts -->
            <div class="container">
                <!-- row starts -->
                <div class="row">
                    <!-- col-md-12 starts -->
                    <div class="col-md-12">
                        <div class="breadcrumb_title_wrapper">
                            <div class="page_title">
                                <h1>Gallery</h1>
                            </div>
                            <ul class="bread_crumb">
                                <li><a href="/">Home</a></li>
                                <li class="bread_active">Gallery</li>
                            </ul>
                        </div>
                    </div><!-- col-md-12 ends -->
                </div>
                <!-- /.row ends -->
            </div><!-- /.container ends -->
        </div>

        <!-- menu starts -->
        @include('UI.common.menu')
        <!-- menu ends -->
    </section>
    <!--================================
        END BREADCRUMB AREA
    =================================-->

    <!--================================
        START QUOTE AREA
    =================================-->
    <section class="get_quote section_padding reveal animated" data-delay="0.2s" data-anim="fadeInUpShort">
        <!-- container starts -->
        <div class="container">
            <!-- row starts -->
            <div class="row">
        @if(isset($Gallery))
        @foreach ($Gallery as $Galleries)
            <div class="col-md-4">
               <a href="/Admin/gallery/{{ $Galleries->image }}" data-lightbox="photos"><img class="img-fluid width100" src="/Admin/gallery/{{ $Galleries->image }}"></a>
            </div>

            
        @endforeach
        
        @endif
    </div>
            <!-- /.row ends -->
        </div><!-- container ends -->
    </section>
    <!--================================
        END QUOTE AREA
    =================================-->

    @endsection
    