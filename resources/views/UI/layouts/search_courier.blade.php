
@extends('UI.base')
@section('Content')
 <!--================================
        START BREADCRUMB AREA
    =================================-->
    <section class="breadcrumb">

      <div class="breadcrumb_content">
          <!-- container starts -->
          <div class="container">
              <!-- row starts -->
              <div class="row">
                  <!-- col-md-12 starts -->
                  <div class="col-md-12">
                      <div class="breadcrumb_title_wrapper">
                          <div class="page_title">
                              <h1>track & trace</h1>
                          </div>
                          <ul class="bread_crumb">
                              <li><a href="/">Home</a></li>
                              <li class="bread_active">Tracking</li>
                          </ul>
                      </div>
                  </div><!-- col-md-12 ends -->
              </div>
              <!-- /.row ends -->
          </div><!-- /.container ends -->
      </div>

      <!-- menu starts -->
      @include('UI.common.menu')
      <!-- menu ends -->
  </section>
  <!--================================
      END BREADCRUMB AREA
  =================================-->

  <!--================================
      START TRACK & TRACE AREA
  =================================-->
  <section class="tc_section section_padding reveal animated" data-delay="0.2s" data-anim="fadeInUpShort">
      <div class="container">
          <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="tc_title"><h4>We are fast & safe delivery your goods everytime on your door steps</h4></div>
                  <div class="tc_form">
                      <form action="/search_courier" method="post">
                        @csrf
                          <div class="tc_input_wrapper">
                              <input name="bill_no" type="text" placeholder="Track your bill no" required>
                              {{-- <span class="fa fa-truck"></span> --}}
                              <button class="tc_btn" type="submit">Search</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
      <br>
      <div class="table-responsive">
      <table class="table table-bordered table-checkable" id="CourierList">

         <thead>
            @if(count($Courier) < 1)
                <tr>
                    <td>No Data Found</td>
                </tr>
            @else
             <tr>
                <th>Company Name</th>
                <th>Pickup Date</th>
                 <th>Bill No</th>
                 <th>Consignment No</th>
                 <th>Cargo Mode</th>
                 <th>Origin </th>
                 <th>Destination</th>
                 <th>Sender</th>
                 <th>Receiver Name</th>
                 <th>Total Pcs</th>
                 <th>Pc Weight</th>
                 <th>Total Weight</th>
                 <th>Courier Status</th>
                 <th>Assured Delivery Date</th>
                 <th>Delivered Date</th>
                 <th>Hub Id</th>
                 <th>Doucket No</th>
                 <th>Remarks</th>
             </tr>
              @endif
         </thead>

         <tbody>
            @if($Courier)
                @foreach($Courier as $Couriers)
                    <tr>
                        <td>{{ $Couriers->company_name }}</td>
                        <td>{{ date('d-m-Y', strtotime($Couriers->pickup_date)) }}</td>
                        <td>{{ $Couriers->bill_no }}</td>
                        <td>{{ $Couriers->consignment_no }}</td>
                        <td>{{ $Couriers->cargo_mode }}</td>
                        <td>{{ $Couriers->origin }}</td>
                        <td>{{ $Couriers->destination }}</td>
                        <td>{{ $Couriers->sender }}</td>
                        <td>{{ $Couriers->receiver_name }}</td>
                        <td>{{ $Couriers->total_pcs }}</td>
                        <td>{{ $Couriers->pc_weight }}</td>
                        <td>{{ $Couriers->total_weight }}</td>
                        <td>{{ $Couriers->courier_status }}</td>
                        <td>{{ date('d-m-Y', strtotime($Couriers->assured_delivery)) }}</td>
                        <td>{{ date('d-m-Y', strtotime($Couriers->delivery_date)) }}</td>
                        <td><a href="{{ $Couriers->web_id }}" target="_blank">{{ $Couriers->web_id }}</a></td>
                        <td>{{ $Couriers->doucket_no }}</td>
                        <td>{{ $Couriers->remarks }}</td>
                @endforeach
           @else
                    <tr>
                        <td>No Data Found...</td>
                    </tr>
           @endif
        </tbody>
     </table>
      </div>
  </section>
  <!--================================
      END TRACK & TRACE AREA
  =================================-->


@endsection
