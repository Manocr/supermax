<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from trusttransport.themeebit.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Dec 2020 05:25:28 GMT -->
<head>
    <meta charset="UTF-8">

    <!-- viewport meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title }}</title>

    <!-- owl carousel css -->
    <link rel="stylesheet" href="{{ URL::asset('UI/css/owl.carousel.css') }}"/>


    <!-- font icofont -->
    <link rel="stylesheet" href="{{ URL::asset('UI/css/font-awesome.min.css') }}"/>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700|Montserrat:300,400,400i,700,900" rel="stylesheet">

    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ URL::asset('UI/css/bootstrap.min.css') }}"/>

    <!-- animte css -->
    <link rel="stylesheet" href="{{ URL::asset('UI/css/animate.css') }}"/>

    <!-- camera css goes here -->
    <link rel="stylesheet" href="{{ URL::asset('UI/css/camera.css') }}">

    <!-- venobox css goes here -->
    <link rel="stylesheet" href="{{ URL::asset('UI/css/venobox.css') }}">

    <!-- style css -->
    <link rel="stylesheet" href="{{ URL::asset('UI/style.css') }}"/>

    <!-- responsive css -->
    <link rel="stylesheet" href="{{ URL::asset('UI/css/responsive.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
    <!-- Favicon -->
    @yield('CssScript')
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
</head>
<body class="home1">

    <!-- preloader -->
    <div class="preloader-bg">
        <div class="preloader-container">
            <div class="my-preloader"><img src="{{ URL::asset('UI/images/logo.jpg') }}" alt="preloader"></div>
        </div>
    </div>


    <!--================================
        START HEADER AREA
    =================================-->

    <!-- start header -->
    @include('UI.common.header')
    <!-- start header -->
    <!--================================
        END HEADER AREA
    =================================-->

   @yield('Content')

    <!--================================
        START FOOTER
    =================================-->
    @include('UI.common.footer')
    <!--================================
        END FOOTER
    =================================-->

    <!--//////////////////// JS GOES HERE ////////////////-->

    <!-- jquery latest version -->
    <script src="{{ URL::asset('UI/js/jquery-1.12.3.js') }}"></script>
    <script src="{{ URL::asset('UI/js/bootstrap.min.js') }}"></script>

    <!-- jquery easing 1.3 -->
    <script src="{{ URL::asset('UI/js/jquery.easing1.3.js') }}"></script>

    <!-- Owl carousel js-->
    <script src="{{ URL::asset('UI/js/owl.carousel.min.js') }}"></script>

    <!-- venobox js -->
    <script src="{{ URL::asset('UI/js/venobox.min.js') }}"></script>

    <!-- Isotope js-->
    <script src="{{ URL::asset('UI/js/isotope.js') }}"></script>

    <!-- Pakcery layout js-->
    <script src="{{ URL::asset('UI/js/packery.js') }}"></script>

    <!-- waypoint js -->
    <script src="{{ URL::asset('UI/js/waypoints.min.js') }}"></script>

    <!-- google map js -->
    <script src="http://maps.googleapis.com/maps/api/js"></script>

    <!-- smoothscroll js -->
    <script src="{{ URL::asset('UI/js/jqury.smooth-scroll.min.js') }}"></script>

    <!-- jquery camera slider js -->
    <script src="{{ URL::asset('UI/js/jquery.camera.min.js') }}"></script>
    <!-- Counter up -->
    <script src="{{ URL::asset('UI/js/jquery.counterup.js') }}"></script>

    <!-- Waypoint -->
    <script src="{{ URL::asset('UI/js/waypoints.min.js') }}"></script>

    <!-- Main js -->
    <script src="{{ URL::asset('UI/js/main.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>



    @yield('JSScript')
</body>

<!-- Mirrored from trusttransport.themeebit.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Dec 2020 05:26:39 GMT -->
</html>
