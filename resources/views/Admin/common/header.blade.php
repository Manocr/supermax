<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
    <!--begin::Header Menu-->
    <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
        <!--begin::Header Nav-->
        <ul class="menu-nav">
            <li class="menu-item menu-item-submenu menu-item-rel">
                <a href="/admin/dashboard" class="menu-link ">
                    <span class="menu-text">Home</span>
                </a>

            </li>

            <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
                <a href="javascript:;" class="menu-link menu-toggle">
                    <span class="menu-text">Courier Management</span>
                </a>

                <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                    <ul class="menu-subnav">
                        <li class="menu-item" aria-haspopup="true">
                            <a href="/courier/list" class="menu-link">
                                <span class="menu-text">Courier List</span>
                                <span class="menu-desc"></span>
                            </a>
                        </li>
                        <li class="menu-item" aria-haspopup="true">
                            <a href="/courier/add_courier" class="menu-link">
                                <span class="menu-text">New Entry</span>
                                <span class="menu-desc"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="menu-item  menu-item-rel">
                <a href="/admin/contact_list" class="menu-link">
                    <span class="menu-text">Contact Leads</span>
                </a>

            </li>

            <li class="menu-item  menu-item-rel">
                <a href="/gallery/list" class="menu-link">
                    <span class="menu-text">Gallery</span>
                </a>

            </li>

            <li class="menu-item  menu-item-rel">
                <a href="/admin/offers_list" class="menu-link">
                <span class="menu-text">Offers</span>
                </a>

            </li>

        </ul>
        <!--end::Header Nav-->
    </div>
    <!--end::Header Menu-->
</div>
