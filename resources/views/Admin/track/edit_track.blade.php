@extends('Admin.base')
@section('Content')
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            <!--begin::Header-->
            <div id="kt_header" class="header header-fixed">
                <!--begin::Container-->
                <div class="container d-flex align-items-stretch justify-content-between">
                    <!--begin::Left-->
                    <div class="d-flex align-items-stretch mr-3">
                        <!--begin::Header Logo-->
                        <div class="header-logo">
                            <a href="../../../index.html">
                                <img alt="Logo" src="{{ URL::asset('UI/images/logo.jpg') }}" class="logo-default max-h-40px" />
                                <img alt="Logo" src="{{ URL::asset('UI/images/logo.jpg') }}" class="logo-sticky max-h-40px" />
                            </a>
                        </div>
                        <!--end::Header Logo-->
                        <!--begin::Header Menu Wrapper-->
                        @include('Admin.common.header')
                        <!--end::Header Menu Wrapper-->
                    </div>
                    <!--end::Left-->
                    <!--begin::Topbar-->
                    @include('Admin.common.top_bar')
                    <!--end::Topbar-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Header-->
            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Subheader-->
                <div class="subheader py-2 py-lg-12 subheader-transparent" id="kt_subheader">
                    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <!--begin::Info-->
                        <div class="d-flex align-items-center flex-wrap mr-1">
                            <!--begin::Heading-->
                            <div class="d-flex flex-column">
                                <!--begin::Title-->
                                <h2 class="text-white font-weight-bold my-2 mr-5">Edit Courier</h2>
                                <!--end::Title-->
                                <!--begin::Breadcrumb-->
                                <!--end::Breadcrumb-->
                            </div>
                            <!--end::Heading-->
                        </div>
                        <!--end::Info-->
                        <!--begin::Toolbar-->
                        {{-- <div class="d-flex align-items-center">
                            <!--begin::Button-->
                            <a href="#" class="btn btn-transparent-white font-weight-bold py-3 px-6 mr-2">Reports</a>
                            <!--end::Button-->
                            <!--begin::Dropdown-->
                            <div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="top">
                                <a href="#" class="btn btn-white font-weight-bold py-3 px-6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</a>
                                <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
                                    <!--begin::Navigation-->
                                    <ul class="navi navi-hover py-5">
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="flaticon2-drop"></i>
                                                </span>
                                                <span class="navi-text">New Group</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="flaticon2-list-3"></i>
                                                </span>
                                                <span class="navi-text">Contacts</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="flaticon2-rocket-1"></i>
                                                </span>
                                                <span class="navi-text">Groups</span>
                                                <span class="navi-link-badge">
                                                    <span class="label label-light-primary label-inline font-weight-bold">new</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="flaticon2-bell-2"></i>
                                                </span>
                                                <span class="navi-text">Calls</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="flaticon2-gear"></i>
                                                </span>
                                                <span class="navi-text">Settings</span>
                                            </a>
                                        </li>
                                        <li class="navi-separator my-3"></li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="flaticon2-magnifier-tool"></i>
                                                </span>
                                                <span class="navi-text">Help</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="flaticon2-bell-2"></i>
                                                </span>
                                                <span class="navi-text">Privacy</span>
                                                <span class="navi-link-badge">
                                                    <span class="label label-light-danger label-rounded font-weight-bold">5</span>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <!--end::Navigation-->
                                </div>
                            </div>
                            <!--end::Dropdown-->
                        </div> --}}
                        <!--end::Toolbar-->
                    </div>
                </div>
                <!--end::Subheader-->
                <!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <!--begin::Card-->
                                <div class="card card-custom gutter-b example example-compact">
                                    <div class="card-header">
                                        {{-- <h3 class="card-title">Base Controls</h3> --}}
                                    </div>
                                    <!--begin::Form-->
                                    @if(session('message'))
                                        <div class="alert alert-success">
                                            <ul>
                                                <li>{!! session('message') !!}</li>
                                            </ul>
                                        </div>
                                    @endif
                                    <form action="/courier/update_courier" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="card-body col-md-6">
                                                <div class="form-group">
                                                    <label>Company Name
                                                    {{-- <span class="text-danger">*</span> --}}
                                                </label>
                                                    <input type="text" name="company_name" class="form-control" value="{{ $Courier->company_name }}"/>
                                                    {{-- <span class="form-text text-muted">We'll never share your email with anyone else.</span> --}}
                                                </div>

                                                <div class="form-group">
                                                    <label>Pickup Date
                                                    {{-- <span class="text-danger">*</span> --}}
                                                </label>
                                                <input type="hidden" name="id" class="form-control" value="{{ $Courier->id }}"/>
                                                <input type="date" name="pickup_date" class="form-control" value="{{ $Courier->pickup_date }}"/>
                                                    {{-- <span class="form-text text-muted">We'll never share your email with anyone else.</span> --}}
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Bill No
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="number" name="bill_no" class="form-control" value="{{ $Courier->bill_no }}" id="exampleInputPassword1" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Consignment No
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="consignment_no" class="form-control" value="{{ $Courier->consignment_no }}" id="exampleInputPassword1" />
                                                    <br>
                                                    <input type="text" name="change_consignment_no" class="form-control" value="" placeholder="Enter New Consignment No" />
                                                </div>

                                                <div class="form-group mb-1">
                                                    <label for="exampleTextarea">Cargo Mode
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="cargo_mode" class="form-control" value="{{ $Courier->cargo_mode }}" id="exampleInputPassword1"  />
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Origin
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="origin" value="Kuwait" class="form-control" value="{{ $Courier->origin }}" id="exampleInputPassword1" disabled />
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Destination
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="destination" class="form-control" value="{{ $Courier->destination }}" id="exampleInputPassword1"  />
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Sender Name
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="sender" class="form-control" value="{{ $Courier->sender }}" id="exampleInputPassword1"  />
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Sender Address
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <textarea class="form-control" name="sender_address" id="exampleTextarea" rows="3">{{ $Courier->sender_address }}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Sender Mobile
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="sender_mobile" class="form-control" value="{{ $Courier->sender_mobile }}" id="exampleInputPassword1"  />
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Receiver Address
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <textarea class="form-control" name="receiver_address" id="exampleTextarea" rows="3">{{ $Courier->receiver_address }}</textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Receiver Name
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="receiver_name" class="form-control" value="{{ $Courier->receiver_name }}" id="exampleInputPassword1"  />
                                                </div>


                                                <!--begin: Code-->


                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Receiver Mobile
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="receiver_mobile" class="form-control" value="{{ $Courier->receiver_mobile }}" id="exampleInputPassword1"  />
                                                </div>
                                            </div>

                                            <div class="card-body col-md-6">


                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Total Pcs
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="total_pcs" class="form-control" value="{{ $Courier->total_pcs }}" id="exampleInputPassword1"  />
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">PC Weight
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="pc_weight" class="form-control" value="{{ $Courier->pc_weight }}" id="exampleInputPassword1"  />
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Total Weight </label>
                                                    <input type="text" name="total_weight" class="form-control" value="{{ $Courier->total_weight }}" id="exampleInputPassword1"  />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Amount
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="amount" id="amount" class="form-control total_weight" value="{{ $Courier->amount }}" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Customs
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="customs" id="customs" class="form-control total_weight" value="{{ $Courier->customs }}" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Insurance
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="insurance" id="insurance" class="form-control total_weight" value="{{ $Courier->insurance }}" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Air/Sea Port Tax(GST)
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="air_sea" id="air_sea" class="form-control total_weight" value="{{ $Courier->air_sea }}" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Documents
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="documents" id="documents"  class="form-control total_weight" value="{{ $Courier->documents }}" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Packing
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="packing" id="packing" class="form-control total_weight" value="{{ $Courier->packing }}" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Carton
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="carton" id="carton" class="form-control total_weight" value="{{ $Courier->carton }}" />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Total Amount
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" id="total_amount"  name="total_amount" value="{{ $Courier->total_amount }}" class="form-control"  />
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Select Status
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" name="courier_status" value="{{ $Courier->courier_status }}" id="StatusValue" class="form-control"  />
                                                    <br>
                                                    <ul>
                                                        @foreach($CourierStatus as $Couriers)
                                                            <li><a href="javascript:void(0);" id="AddCourierStatus" data-id="{{ $Couriers->courier_status }}">{{ $Couriers->courier_status }}</a> </li>
                                                        @endforeach
                                                    </ul>
                                                </div>



                                                {{-- <div class="form-group">
                                                    <label>Select Status</label>
                                                    <select class="form-control form-control-solid" name="courier_status">
                                                        <option value="In Transit">In Transit</option>
                                                        <option value="Delivered">Delivered</option>
                                                    </select>
                                                </div> --}}

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Assured Delivery Date
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="date" class="form-control" name="assured_delivery" value="{{ $Courier->assured_delivery }}" id="exampleInputPassword1"  />
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Delivery Date
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="date" name="delivery_date" class="form-control" value="{{ $Courier->delivery_date }}" id="exampleInputPassword1"  />
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Remarks
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <textarea class="form-control" name="remarks" id="exampleTextarea" rows="3">{{ $Courier->remarks }}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Hub Web Id
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" class="form-control" name="web_id" value="{{ $Courier->web_id }}" id="exampleInputPassword1"  />
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Doucket No
                                                    {{-- <span class="text-danger">*</span> --}}</label>
                                                    <input type="text" class="form-control" name="doucket_no" value="{{ $Courier->doucket_no }}" id="exampleInputPassword1"  />
                                                </div>
                                                <!--begin: Code-->

                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary mr-2 btn-green" name="action" value="update">Submit</button>
                                            <button type="submit" class="btn btn-primary mr-2" name="action" value="update_all_records">Change Consignment Status</button>
                                            <button type="submit" class="btn btn-primary mr-2" name="action" value="update_all_consignement">Change Consignment No</button>
                                            {{-- <button type="reset" class="btn btn-secondary">Cancel</button> --}}
                                        </div>
                                    </form>
                                    <!--end::Form-->
                                </div>
                                <!--end::Card-->
                                <!--begin::Card-->

                                <!--end::Card-->
                                <!--begin::Card-->

                                <!--end::Card-->
                            </div>

                        </div>
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->
            </div>
            <!--end::Content-->
            <!--begin::Footer-->
            <div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
                <!--begin::Container-->
                <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted font-weight-bold mr-2">2020©</span>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">Keenthemes</a>
                    </div>
                    <!--end::Copyright-->
                    <!--begin::Nav-->
                    <div class="nav nav-dark order-1 order-md-2">
                        <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pr-3 pl-0">About</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link px-3">Team</a>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pl-3 pr-0">Contact</a>
                    </div>
                    <!--end::Nav-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
@endsection

@section('JSScript')
    <script>
        $(document).on("click", "#AddCourierStatus", function(){
            var id = $(this).data('id');

            $("#StatusValue").val(id);
        });

        $(function(){
            $('#amount, #customs, #insurance, #air_sea, #documents, #packing, #carton').keyup(function(){
               var amount = parseFloat($('#amount').val()) || 0;
               var customs = parseFloat($('#customs').val()) || 0;
               var insurance = parseFloat($('#insurance').val()) || 0;
               var air_sea = parseFloat($('#air_sea').val()) || 0;
               var documents = parseFloat($('#documents').val()) || 0;
               var packing = parseFloat($('#packing').val()) || 0;
               var carton = parseFloat($('#carton').val()) || 0;
               $('#total_amount').val(amount + customs + insurance + air_sea + documents + packing + carton);
            });
         });
    </script>
@endsection
