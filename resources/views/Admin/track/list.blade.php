@extends('Admin.base')
@section('Content')
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            <!--begin::Header-->
            <div id="kt_header" class="header header-fixed">
                <!--begin::Container-->
                <div class="container d-flex align-items-stretch justify-content-between">
                    <!--begin::Left-->
                    <div class="d-flex align-items-stretch mr-3">
                        <!--begin::Header Logo-->
                        <div class="header-logo">
                            <a href="../../../index.html">
                                <img alt="Logo" src="{{ URL::asset('UI/images/logo.jpg') }}" class="logo-default max-h-40px" />
                                <img alt="Logo" src="{{ URL::asset('UI/images/logo.jpg') }}" class="logo-sticky max-h-40px" />
                            </a>
                        </div>
                        <!--end::Header Logo-->
                        <!--begin::Header Menu Wrapper-->
                        @include('Admin.common.header')
                        <!--end::Header Menu Wrapper-->
                    </div>
                    <!--end::Left-->
                    <!--begin::Topbar-->
                    @include('Admin.common.top_bar')

                    <!--end::Topbar-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Header-->
            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Subheader-->
                <div class="subheader py-2 py-lg-12 subheader-transparent" id="kt_subheader">
                    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <!--begin::Info-->
                        <div class="d-flex align-items-center flex-wrap mr-1">
                            <!--begin::Heading-->
                            <div class="d-flex flex-column">
                                <!--begin::Title-->
                                <h2 class="text-white font-weight-bold my-2 mr-5">{{ $title }}</h2>
                                <!--end::Title-->
                                <!--begin::Breadcrumb-->
                                {{-- <div class="d-flex align-items-center font-weight-bold my-2">
                                    <!--begin::Item-->
                                    <a href="#" class="opacity-75 hover-opacity-100">
                                        <i class="flaticon2-shelter text-white icon-1x"></i>
                                    </a>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Crud</a>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Datatables.net</a>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Basic</a>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Basic Tables</a>
                                    <!--end::Item-->
                                </div> --}}
                                <!--end::Breadcrumb-->
                            </div>
                            <!--end::Heading-->
                        </div>
                        <!--end::Info-->
                        <!--begin::Toolbar-->
                        <div class="d-flex align-items-center">
                            <!--begin::Button-->
                            {{-- <a href="#" class="btn btn-transparent-white font-weight-bold py-3 px-6 mr-2">Reports</a> --}}
                            <a href="/courier/add_courier" class="btn btn-transparent-white font-weight-bold py-3 px-6 mr-2">New Entry</a>
                            {{-- <a href="#" class="btn btn-transparent-white font-weight-bold py-3 px-6 mr-2">Update Remarks</a> --}}
                            <!--end::Button-->
                            <!--begin::Dropdown-->
                            <!--end::Dropdown-->
                        </div>
                        <!--end::Toolbar-->
                    </div>
                </div>
                <!--end::Subheader-->
                <!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container-fluid">
                        <!--begin::Notice-->
                        <!--end::Notice-->
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b">
                            {{-- <div class="card-header flex-wrap py-3">
                                <div class="card-title">
                                    <h3 class="card-label">Basic Demo
                                </div>
                            </div> --}}
                            <div class="card-body">
                                <!--begin: Datatable-->
                                @if(session('message'))
                                        <div class="alert alert-success">
                                            <ul>
                                                <li>{!! session('message') !!}</li>
                                            </ul>
                                        </div>
                                    @endif
                                <table class="table table-bordered table-checkable table-responsive" id="CourierList">
                                    <thead>
                                        <tr>
                                            {{-- <th>Id</th> --}}
                                            <th>Actions</th>
                                            <th>Company Name</th>
                                            <th>Pickup Date</th>
                                            <th>Bill No</th>
                                            <th>Consignment No</th>
                                            <th>Cargo Mode</th>
                                            <th>Origin </th>
                                            <th>Destination</th>
                                            <th>Sender</th>
                                            <th>Sender Mobile</th>
                                            <th>Sender Address</th>
                                            <th>Receiver Name</th>
                                            <th>Receiver Address</th>
                                            <th>Receiver Mobile</th>
                                            <th>Total Pcs</th>
                                            <th>Pc Weight</th>
                                            <th>Total Weight</th>
                                            <th>Amount</th>
                                            <th>Customs</th>
                                            <th>Insurance</th>
                                            <th>Air/Sea Port Tax(GST)</th>
                                            <th>Documents</th>
                                            <th>Packing</th>
                                            <th>Carton</th>
                                            <th>Total Amount</th>
                                            <th>Courier Status</th>
                                            <th>Delivered Date</th>
                                            <th>Assured Delivery Date</th>
                                            <th>Remarks</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($Courier)
                                            @foreach($Courier as $Couriers)
                                                <tr>
                                                    {{-- <td>{{ $Couriers->id }}</td> --}}
                                                    <td nowrap="nowrap">	                        	                        <a href="/courier/edit_courier/{{ $Couriers->id }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">	                            <span class="svg-icon svg-icon-md">	                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">	                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">	                                        <rect x="0" y="0" width="24" height="24"></rect>	                                        <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path>	                                        <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect>	                                    </g>	                                </svg>	                            </span>	                        </a>	                        <a href="/courier/delete_courier/{{ $Couriers->id }}" onclick="return confirm(' Are you sure. You want to delete?');" class="btn btn-sm btn-clean btn-icon" title="Delete">	                            <span class="svg-icon svg-icon-md">	                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">	                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">	                                        <rect x="0" y="0" width="24" height="24"></rect>	                                        <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>	                                        <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>	                                    </g>	                                </svg>	                            </span>	                        </a>	                    </td>
                                                    <td>{{ $Couriers->company_name }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($Couriers->pickup_date)) }}</td>
                                                    <td>{{ $Couriers->bill_no }}</td>
                                                    <td>{{ $Couriers->consignment_no }}</td>
                                                    <td>{{ $Couriers->cargo_mode }}</td>
                                                    <td>{{ $Couriers->origin }}</td>
                                                    <td>{{ $Couriers->destination }}</td>
                                                    <td>{{ $Couriers->sender }}</td>
                                                    <td>{{ $Couriers->sender_mobile }}</td>
                                                    <td>{{ $Couriers->sender_address }}</td>
                                                    <td>{{ $Couriers->receiver_name }}</td>
                                                    <td>{{ $Couriers->receiver_address }}</td>
                                                    <td>{{ $Couriers->receiver_mobile }}</td>
                                                    <td>{{ $Couriers->total_pcs }}</td>
                                                    <td>{{ $Couriers->pc_weight }}</td>
                                                    <td>{{ $Couriers->total_weight }}</td>
                                                    <td>{{ $Couriers->amount }}</td>
                                                    <td>{{ $Couriers->customs }}</td>
                                                    <td>{{ $Couriers->insurance }}</td>
                                                    <td>{{ $Couriers->air_sea }}</td>
                                                    <td>{{ $Couriers->documents }}</td>
                                                    <td>{{ $Couriers->packing }}</td>
                                                    <td>{{ $Couriers->carton }}</td>
                                                    <td>{{ $Couriers->total_amount }}</td>
                                                    <td>{{ $Couriers->courier_status }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($Couriers->delivery_date)) }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($Couriers->assured_delivery)) }}</td>
                                                    <td>{{ $Couriers->remarks }}</td>

                                                </tr>
                                            @endforeach
                                       @else
                                                <tr>
                                                    <td>No Data Found...</td>
                                                </tr>
                                       @endif
                                    </tbody>
                                </table>
                                <!--end: Datatable-->
                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->
            </div>
            <!--end::Content-->
            <!--begin::Footer-->
            <div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
                <!--begin::Container-->
                <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted font-weight-bold mr-2">2020©</span>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">Keenthemes</a>
                    </div>
                    <!--end::Copyright-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
@endsection


@section('JSScript')
    <script>
        $(document).ready( function () {
            $('#CourierList').DataTable( {
        dom: 'Bfrtip',
        scrollX: true,
        buttons: [
            'csv', 'excel'
        ],
    } );

    $(document).on('keydown', function ( e ) {
    // You may replace `c` with whatever key you want
    if ((e.metaKey || e.ctrlKey) && ( String.fromCharCode(e.which).toLowerCase() === 'u') ) {
        // console.log( "You pressed CTRL + C" );
        $('input[type="search"]').focus()
    }
});
    // $('#CourierList').DataTable();
} );
    </script>
@endsection
