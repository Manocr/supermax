@extends('Admin.base')
@section('Content')
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            <!--begin::Header-->
            <div id="kt_header" class="header header-fixed">
                <!--begin::Container-->
                <div class="container d-flex align-items-stretch justify-content-between">
                    <!--begin::Left-->
                    <div class="d-flex align-items-stretch mr-3">
                        <!--begin::Header Logo-->
                        <div class="header-logo">
                            <a href="../../../index.html">
                                <img alt="Logo" src="{{ URL::asset('UI/images/logo.jpg') }}" class="logo-default max-h-40px" />
                                <img alt="Logo" src="{{ URL::asset('UI/images/logo.jpg') }}" class="logo-sticky max-h-40px" />
                            </a>
                        </div>
                        <!--end::Header Logo-->
                        <!--begin::Header Menu Wrapper-->
                        @include('Admin.common.header')
                        <!--end::Header Menu Wrapper-->
                    </div>
                    <!--end::Left-->
                    <!--begin::Topbar-->
                    @include('Admin.common.top_bar')

                    <!--end::Topbar-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Header-->
            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Subheader-->
                <div class="subheader py-2 py-lg-12 subheader-transparent" id="kt_subheader">
                    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <!--begin::Info-->
                        <div class="d-flex align-items-center flex-wrap mr-1">
                            <!--begin::Heading-->
                            <div class="d-flex flex-column">
                                <!--begin::Title-->
                                <h2 class="text-white font-weight-bold my-2 mr-5">{{ $title }}</h2>
                                <!--end::Title-->
                                <!--begin::Breadcrumb-->
                                {{-- <div class="d-flex align-items-center font-weight-bold my-2">
                                    <!--begin::Item-->
                                    <a href="#" class="opacity-75 hover-opacity-100">
                                        <i class="flaticon2-shelter text-white icon-1x"></i>
                                    </a>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Crud</a>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Datatables.net</a>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Basic</a>
                                    <!--end::Item-->
                                    <!--begin::Item-->
                                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                                    <a href="#" class="text-white text-hover-white opacity-75 hover-opacity-100">Basic Tables</a>
                                    <!--end::Item-->
                                </div> --}}
                                <!--end::Breadcrumb-->
                            </div>
                            <!--end::Heading-->
                        </div>
                        <!--end::Toolbar-->
                    </div>
                </div>
                <!--end::Subheader-->
                <!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container-fluid">
                        <!--begin::Notice-->
                        <!--end::Notice-->
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b">
                            {{-- <div class="card-header flex-wrap py-3">
                                <div class="card-title">
                                    <h3 class="card-label">Basic Demo
                                </div>
                            </div> --}}
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        @if(session('message'))
                                        <div class="alert alert-success">
                                            <ul>
                                                <li>{!! session('message') !!}</li>
                                            </ul>
                                        </div>
                                    @endif
                                    <form action="/update_offers" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="card-body col-md-6">
                                                <div class="form-group">
                                                    <label>Upload Image
                                                    {{-- <span class="text-danger">*</span> --}}
                                                </label>
                                                <input type="hidden" name="id" value="{{ $Offers->id }}">
                                                    <input type="file" name="image" class="form-control" />
                                                    {{-- <span class="form-text text-muted">We'll never share your email with anyone else.</span> --}}
                                                </div>

                                            </div>

                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary mr-2">Submit</button>

                                                <input data-id="{{$Offers->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $Offers->status ? 'checked' : '' }}>
                                        </div>
                                    </form>
                                    </div>
                                </div>

                                <div class="row">
                                        <div class="col-md-3">
                                            <img src="/Admin/offers/{{ $Offers->image }}" alt="" style="
                                            width: 350px;
                                        ">
                                        </div>

                                </div>

                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->
            </div>
            <!--end::Content-->
            <!--begin::Footer-->
            <div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
                <!--begin::Container-->
                <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted font-weight-bold mr-2">2020©</span>
                        <a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">Keenthemes</a>
                    </div>
                    <!--end::Copyright-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
@endsection


@section('JSScript')
    <script>
        $(document).ready( function () {
    $('#CourierList').DataTable();
} );
    </script>

<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});




    $(function() {
      $('.toggle-class').change(function() {
          var status = $(this).prop('checked') == true ? 1 : 0;
          var id = $(this).data('id');
           console.log(status);
          $.ajax({
              type: "POST",
              dataType: "json",
              url: '/ChangeStatus',
              data: {'status': status, 'id': id},
              success: function(data){
                console.log(data.success)
                alert(data.success);
              }
          });
      })
    })
  </script>
@endsection


