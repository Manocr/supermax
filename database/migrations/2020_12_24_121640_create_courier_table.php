<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courier', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
            $table->string('pickup_date');
            $table->string('bill_no');
            $table->string('consignment_no');
            $table->string('cargo_mode');
            $table->string('origin');
            $table->string('destination');
            $table->string('sender');
            $table->string('sender_mobile');
            $table->string('sender_address');
            $table->string('receiver_name');
            $table->longText('receiver_address');
            $table->string('receiver_mobile');
            $table->string('total_pcs');
            $table->string('pc_weight');
            $table->string('total_weight');
            $table->string('amount');
            $table->string('customs');
            $table->string('insurance');
            $table->string('air_sea');
            $table->string('documents');
            $table->string('packing');
            $table->string('carton');
            $table->string('total_amount');
            $table->string('courier_status');
            $table->string('delivery_date');
            $table->string('assured_delivery');
            $table->longText('remarks');
            $table->string('web_id');
            $table->string('doucket_no');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courier');
    }
}
