-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2020 at 02:24 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `courier`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_leads`
--

CREATE TABLE `contact_leads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_leads`
--

INSERT INTO `contact_leads` (`id`, `name`, `email`, `mobile`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Manoj', 'mano@gmail.com', '9876432102', 'dsaewrewrew', '2020-12-25 03:39:18', '2020-12-25 03:39:18'),
(2, 'Learn', 'mano@gmail.com', '9876542332', 'dsarewrsfsd', '2020-12-28 06:38:40', '2020-12-28 06:38:40');

-- --------------------------------------------------------

--
-- Table structure for table `courier`
--

CREATE TABLE `courier` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pickup_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consignment_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo_mode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `origin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_mobile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_mobile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_pcs` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pc_weight` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_weight` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_amount` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `courier_status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assured_delivery` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `web_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doucket_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courier`
--

INSERT INTO `courier` (`id`, `pickup_date`, `bill_no`, `consignment_no`, `cargo_mode`, `origin`, `destination`, `sender`, `sender_mobile`, `sender_address`, `receiver_address`, `receiver_mobile`, `total_pcs`, `pc_weight`, `total_weight`, `total_amount`, `courier_status`, `delivery_date`, `assured_delivery`, `remarks`, `web_id`, `doucket_no`, `status`, `created_at`, `updated_at`) VALUES
(1, '2020-12-25', '2133', '5563', 'dsd', 'Kuwait', 'dsfdsfsdf', 'fdsfsd', '9765423255', 'rewrwe\r\nrewr\r\newrwerewrwe', 'rewrwe\r\nrewr\r\newrwerewrwe', '9765423255', '54', '5435', '4354', NULL, 'In Transit', '2020-12-27', '2020-12-26', 'fdsfds\r\nfdsfs', '4tertre', 'sfdfsdfsd', 1, '2020-12-24 07:12:16', '2020-12-28 01:31:39'),
(4, '2020-12-25', '2133', '5563', 'dsd', 'sfdsf', 'dsfdsfsdf', 'fdsfsd', '', NULL, 'rewrwe\r\nrewr\r\newrwerewrwe', '', '54', '5435', '4354', NULL, 'In Transit', NULL, '2020-12-26', 'fdsfds\r\nfdsfs', '4tertre', 'sfdfsdfsd', 1, '2020-12-24 07:12:16', '2020-12-27 10:52:53'),
(5, '2020-12-27', '2135', '5563', 'rewrew', 'Kuwait', 'fsdfsd', 'erwtre', '9765423255', NULL, 'marathahalli', '9765423255', '45', '5435', '4354', NULL, 'In Transit', NULL, '2020-12-28', 'dsafdsgfdgfdgdfgdf', '4tertre', 'sfdfsdfsd', 1, '2020-12-27 10:18:40', '2020-12-27 10:52:53');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `image`, `created_at`, `updated_at`) VALUES
(2, '5fe71cfa9aea6_1608981754.jpg', '2020-12-26 05:52:34', '2020-12-26 05:52:34'),
(3, '5fe71f42e7d3d_1608982338.jfif', '2020-12-26 06:02:18', '2020-12-26 06:02:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_12_24_121640_create_courier_table', 1),
(2, '2014_10_12_000000_create_users_table', 2),
(3, '2020_12_25_085914_create_contact_leads_table', 3),
(4, '2020_12_26_110255_create_gallery_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$zPgKPJF.H3B6UH/PYtxxTu8KOS.pWBiseDC6WEqG1.6PKkxcrMkO2', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_leads`
--
ALTER TABLE `contact_leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courier`
--
ALTER TABLE `courier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_leads`
--
ALTER TABLE `contact_leads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `courier`
--
ALTER TABLE `courier`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
