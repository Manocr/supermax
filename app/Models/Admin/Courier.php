<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    use HasFactory;

    protected $table = 'courier';

    protected $fillable = ['company_name','pickup_date', 'bill_no', 'consignment_no', 'cargo_mode', 'origin','destination', 'sender','sender_mobile', 'sender_address', 'receiver_name', 'receiver_address','receiver_mobile', 'total_pcs', 'pc_weight','total_weight','amount', 'customs','insurance', 'air_sea', 'documents', 'packing', 'carton', 'total_amount', 'courier_status', 'delivery_date', 'assured_delivery', 'remarks', 'web_id', 'doucket_no', 'status'];
}
