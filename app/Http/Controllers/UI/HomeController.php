<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Courier;
use App\Models\UI\Contact;
use App\Models\UI\Gallery;
use App\Models\UI\Offers;

class HomeController extends Controller
{
    public function home(){
        $title = "Super max Cargo";
        $Offers = Offers::where('status', 0)->first();
        return view('UI.layouts.home', compact('title', 'Offers'));
    }

    public function track_courier(Request $request){
        $title = "Super max Cargo";

        return view('UI.layouts.track_courier', compact('title'));
    }

    public function search_courier(Request $request){
        $title = "Super max Cargo";

        $BillNo = $request->bill_no;

        $Courier = Courier::where('bill_no', 'LIKE', "%{$BillNo}%")
                        ->get();

        return view('UI.layouts.search_courier', compact('title', 'Courier'));
    }

    public function services(){
        $title = "Super max Cargo";

        return view('UI.layouts.service', compact('title'));
    }

    public function contact(){
        $title = "Super max Cargo";

        return view('UI.layouts.contact', compact('title'));
    }

    public function store_contact(Request $request){

        $Contact = new Contact();

        $Contact->name = $request->name;
        $Contact->email = $request->email;
        $Contact->mobile = $request->mobile;
        $Contact->message = $request->message;

        $AddContact = $Contact->save();

        return redirect()->back()->with('message','Thank you for contacting supermax cargo. We will get back to you soon...');
    }

    public function gallery_list(){
        $title = "Gallery List";
        $Gallery = Gallery::orderBy('created_at', 'desc')->get();
        return view('UI.layouts.gallery', compact('title', 'Gallery'));
    }
}
