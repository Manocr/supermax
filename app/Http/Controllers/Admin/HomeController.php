<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Courier;
use App\Models\Admin\Users;
use App\Models\UI\Contact;
use App\Models\UI\Gallery;
use App\Models\UI\Offers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

use Mail;

class HomeController extends Controller
{
    public function login(){
        $title = "Admin Login";

        return view('Admin.login', compact('title'));
    }

    public function forgot_mail(Request $request){

        $Email = $request->forgot_email;

        $CheckEmail = Users::where('email', $Email)->first();

        if($CheckEmail){
            $data = array('name'=>"Supermax Cargo");

            Mail::send(['text'=>'mail'], $data, function($message) {
                $message->to($Email, 'Supermax Cargo')->subject
                   ('Forgot mail');
                $message->from($Email,'Supermax Cargo');
             });
             echo "Basic Email Sent. Check your inbox.";
        }
    }


    public function dashboard(){
        $title = "Admin Dashboard";
        $TotalCourier = Courier::count();
        $Courier = Courier::orderBy('created_at', 'desc')->take(10)->get();
        $Totaldelivered = Courier::where('courier_status', 'Delivered')->count();
        return view('Admin.layouts.dashboard', compact('title', 'Totaldelivered', 'Courier', 'TotalCourier'));
    }

    public function admin_login(Request $request){
        $Email = $request->email;
        $Password = $request->password;

        $CheckEmail = Users::where('email', $Email)->first();

        if($CheckEmail == null){

            return redirect()->back()->with('message','Please check your credentials');

        }else{

            if (Auth::guard('super_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                $request->session()->put('AdminName', Auth::guard('super_admin')->user()->name);
                $request->session()->put('AdminEmail', Auth::guard('super_admin')->user()->email);
                $request->session()->put('AdminId', Auth::guard('super_admin')->user()->id);
                // return response()->json(array(
                //     "error"=>FALSE,
                //     "message"=> "Login successfully",
                //     "type" => 1
                // ));
                return redirect('admin/dashboard');
                // echo "Success";
            }else{
                return redirect()->back()->with('message','Please check your credentials');

            }
        }

    }

    public function contact_list(){
        $title = "Contact List";
        $Contact = Contact::orderBy('created_at', 'desc')->get();
        return view('Admin.layouts.contact_leads', compact('title', 'Contact'));
    }

    public function courier_list(){
        $title = "Courier List";
        $Courier = Courier::orderBy('created_at', 'desc')->get();
        return view('Admin.track.list', compact('title', 'Courier'));
    }

    public function add_courier(){
        $title = "Add Courier";
        $Courier = Courier::select('courier_status')
                    ->distinct()
                    ->get();

        $company_name = Courier::select('company_name')
                    ->distinct()
                    ->get();

        $consignment_no = Courier::select('consignment_no')
                    ->distinct()
                    ->limit(5)
                    ->orderBy('created_at', 'desc')
                    ->get();
        $cargo_mode = Courier::select('cargo_mode')
                    ->distinct()
                    ->get();
        // $company_name = Courier::select('company_name')
        //             ->distinct()
        //             ->get();

        return view('Admin.track.add_track', compact('title', 'Courier', 'company_name', 'consignment_no', 'cargo_mode'));
    }

    public function edit_courier($id){
        $title = "Edit Courier";
        $Courier = Courier::where('id', $id)->first();
        $CourierStatus = Courier::select('courier_status')
                    ->distinct()
                    ->get();
        return view('Admin.track.edit_track', compact('title', 'Courier', 'CourierStatus'));
    }

    public function delete_courier($id){
        $Courier = Courier::where('id', $id)->delete();
        return redirect()->back()->with('message','Courier Deleted Successfully');
    }

    public function store_courier(Request $request){

        $Courier = new Courier();

        $Courier->company_name = $request->company_name;
        $Courier->pickup_date = $request->pickup_date;
        $Courier->bill_no = $request->bill_no;
        $Courier->consignment_no = $request->consignment_no;
        $Courier->cargo_mode = $request->cargo_mode;
        $Courier->origin = "Kuwait";
        $Courier->destination = $request->destination;
        $Courier->sender = $request->sender;
        $Courier->sender_mobile = $request->sender_mobile;
        $Courier->sender_address = $request->sender_address;
        $Courier->receiver_name = $request->receiver_name;
        $Courier->receiver_address = $request->receiver_address;
        $Courier->receiver_mobile = $request->receiver_mobile;
        $Courier->total_pcs = $request->total_pcs;
        $Courier->pc_weight = $request->pc_weight;
        $Courier->total_weight = $request->total_weight;
        $Courier->amount = $request->amount;
        $Courier->customs = $request->customs;
        $Courier->insurance = $request->insurance;
        $Courier->air_sea = $request->air_sea;
        $Courier->documents = $request->documents;
        $Courier->packing = $request->packing;
        $Courier->carton = $request->carton;
        $Courier->total_amount = $request->total_amount;
        $Courier->courier_status = $request->courier_status;
        $Courier->delivery_date = $request->assured_delivery;

        if($request->assured_delivery){
            $Courier->assured_delivery = $request->assured_delivery;
        }else{
            $Courier->assured_delivery = "null";
        }


        $Courier->remarks = $request->remarks;
        $Courier->web_id = $request->web_id;
        $Courier->doucket_no = $request->doucket_no;
        $Courier->status = 1;

        $AddCourier = $Courier->save();

        $request->session()->put('CourierId', $Courier->id);

        return redirect()->back()->with('message','Courier Added Successfully');
    }

    public function update_courier(Request $request){

        switch ($request->action) {
            case 'update':
                $id = $request->id;

                $Courier = Courier::where('id', $id)->first();

                $Courier->company_name = $request->company_name;
                $Courier->pickup_date = $request->pickup_date;
                $Courier->bill_no = $request->bill_no;
                $Courier->consignment_no = $request->consignment_no;
                $Courier->cargo_mode = $request->cargo_mode;
                $Courier->origin = "Kuwait";
                $Courier->destination = $request->destination;
                $Courier->sender = $request->sender;
                $Courier->sender_mobile = $request->sender_mobile;
                $Courier->sender_address = $request->sender_address;
                $Courier->receiver_name = $request->receiver_name;
                $Courier->receiver_address = $request->receiver_address;
                $Courier->receiver_mobile = $request->receiver_mobile;
                $Courier->total_pcs = $request->total_pcs;
                $Courier->pc_weight = $request->pc_weight;
                $Courier->total_weight = $request->total_weight;
                $Courier->amount = $request->amount;
        $Courier->customs = $request->customs;
        $Courier->insurance = $request->insurance;
        $Courier->air_sea = $request->air_sea;
        $Courier->documents = $request->documents;
        $Courier->packing = $request->packing;
        $Courier->carton = $request->carton;
                $Courier->total_amount = $request->total_amount;
                $Courier->courier_status = $request->courier_status;
                $Courier->delivery_date = $request->delivery_date;
                if($request->assured_delivery){
                    $Courier->assured_delivery = $request->assured_delivery;
                }else{
                    $Courier->assured_delivery = "null";
                }
                $Courier->remarks = $request->remarks;
                $Courier->web_id = $request->web_id;
                $Courier->doucket_no = $request->doucket_no;

                $AddCourier = $Courier->save();

                return redirect()->back()->with('message','Courier Updated Successfully');
                break;

            case 'update_all_records':
                $consignment_no = $request->consignment_no;

                $Courier = Courier::where('consignment_no', $consignment_no)->get();

                foreach($Courier as $Couriers){
                    // echo json_encode($Couriers);
                    $Couriers->courier_status = $request->courier_status;

                    $Couriers->save();
                }

            return redirect()->back()->with('message','Courier Updated Successfully');
            break;

            case 'update_all_consignement':
                $consignment_no = $request->consignment_no;

                $Courier = Courier::where('consignment_no', $consignment_no)->get();

                foreach($Courier as $Couriers){
                    // echo json_encode($Couriers);
                    $Couriers->consignment_no = $request->change_consignment_no;

                    $Couriers->save();
                }

            return redirect()->back()->with('message','Courier Updated Successfully');
            break;
        }


    }

    public function admin_logout()
    {
        Auth::guard('super_admin')->logout();
        return redirect('/admin/login');
    }

    // Gallery
    public function gallery_list(){
        $title = "Gallery List";
        $Gallery = Gallery::orderBy('created_at', 'desc')->get();
        return view('Admin.gallery.gallery_list', compact('title', 'Gallery'));
    }


    public function store_gallery(Request $request){

        $Gallery = new Gallery();


        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/gallery/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);

            $Gallery->image = $filename;
        }

        $AddGallery = $Gallery->save();

        return redirect()->back()->with('message','Gallery Added Successfully');
    }

    public function delete_gallery($id){
        $Gallery = Gallery::where('id', $id)->delete();
        return redirect()->back()->with('message','Gallery Deleted Successfully');
    }

    public function delete_contact($id){
        $Contact = Contact::where('id', $id)->delete();
        return redirect()->back()->with('message','Contact Deleted Successfully');
    }


    public function offers_list(){
        $title = "Offers List";
        $Offers = Offers::orderBy('created_at', 'desc')->first();
        return view('Admin.gallery.offers_list', compact('title', 'Offers'));
    }


    public function update_offers(Request $request){
        $id = $request->id;

        $Offers = Offers::where('id', $id)->first();
        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/offers/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);

            $Offers->image = $filename;
        }else{
            $Offers->image = $Offers->image;
        }

        $AddOffers = $Offers->save();

        return redirect()->back()->with('message','Offers Added Successfully');

    }

    public function ChangeStatus(Request $request)
    {
    	// \Log::info($request->all());
        $Offers = Offers::find($request->id);
        $Offers->status = $request->status;
        $Offers->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
}
